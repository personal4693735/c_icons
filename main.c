#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "./jsmn.h"
#include "./utils.c"

#define JSON_EQUAL 0
#define STRING_OFFSET 1
#define FALSE_LEN 5
#define MAX_TOKENS 100000
const char* FALSE_STR = "false";

typedef enum {
    SOLID, LINEAR, GRADIENT
} colorMode_t;

enum {
    PROP_LIBRARY_INDEX = 0,
    PROP_ICON_INDEX,
    PROP_WIDTH_INDEX,
    PROP_HEIGHT_INDEX,
    PROP_FILL_MODE_INDEX,
    PROP_COLORS_INDEX,
    ARRAY_SIZE
} propIdx_t;

const char* PROP_LIBRARY = "library";
const char* PROP_ICON = "icon";
const char* PROP_WIDTH = "width";
const char* PROP_HEIGHT = "height";
const char* PROP_FILL_MODE = "fill-mode";
const char* PROP_COLORS = "colors";

const char* KEY_ICONS = "\"icons\":";
const int KEY_ICONS_LEN = 9;
const char* KEY_SUFFIX = "\"suffixes\":";
const int KEY_SUFFIX_LEN = 12;
const char* KEY_BODY = "\"body\":";
const int KEY_BODY_LEN = 8;
const char* KEY_PALETTE = "\"palette\":";
const int KEY_PALETTE_LEN = 11;

const char* FILL_MODE_SOLID = "solid";
const char* FILL_MODE_LINEAR = "linear";
const char* FILL_MODE_RADIAL = "radial";

int main(int argc, char *argv[]) {
    #ifdef DEV_MODE 
        printf("[DEV MODE] Aplicación iniciada.\n");
    #endif
    int i, r;
    char *substr;
    const char* EQUALS = "=";
    char fileName[50] = "./icons/";
    void *properties[ARRAY_SIZE] = { NULL, NULL, NULL, NULL, NULL, NULL };
    
    for(i = 1; i < argc; i++) {
        #ifdef DEV_MODE 
            printf("[DEV MODE] Propiedad \"%s\" leída.\n", argv[i]);
        #endif
        if(strstr(argv[i], PROP_LIBRARY)) {
            properties[PROP_LIBRARY_INDEX] = (argv[i] + strlen(PROP_LIBRARY) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_LIBRARY, (char*) properties[PROP_LIBRARY_INDEX]);
            #endif
        } else if(strstr(argv[i], PROP_ICON)) {
            properties[PROP_ICON_INDEX] = (argv[i] + strlen(PROP_ICON) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_ICON, (char*) properties[PROP_ICON_INDEX]);
            #endif
        } else if(strstr(argv[i], PROP_WIDTH)) {
            properties[PROP_WIDTH_INDEX] = (argv[i] + strlen(PROP_WIDTH) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_WIDTH, (char*) properties[PROP_WIDTH_INDEX]);
            #endif
        } else if(strstr(argv[i], PROP_HEIGHT)) {
            properties[PROP_HEIGHT_INDEX] = (argv[i] + strlen(PROP_HEIGHT) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_HEIGHT, (char*) properties[PROP_HEIGHT_INDEX]);
            #endif
        } else if(strstr(argv[i], PROP_FILL_MODE)) {
            properties[PROP_FILL_MODE_INDEX] = (argv[i] + strlen(PROP_FILL_MODE) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_FILL_MODE, (char*) properties[PROP_FILL_MODE_INDEX]);
            #endif
        } else if(strstr(argv[i], PROP_COLORS)) {
            properties[PROP_COLORS_INDEX] = (argv[i] + strlen(PROP_COLORS) + STRING_OFFSET);
            #ifdef DEV_MODE 
                printf("[DEV MODE] Propiedad \"%s\" con valor \"%s\" asignada.\n", 
                    PROP_COLORS, (char*) properties[PROP_COLORS_INDEX]);
            #endif
        }
    }

    // Check if properties library and icon has values, if not return error
    if(!properties[PROP_LIBRARY_INDEX] || !properties[PROP_ICON_INDEX]) {
        #ifdef DEV_MODE 
            printf("[DEV MODE] No se han indicado ni libreria o icono, ambos son requeridos.\n");
        #endif
        return 300;
    } else {
        strcat((char*)properties[PROP_LIBRARY_INDEX], ".json");
        #ifdef DEV_MODE 
            printf("[DEV MODE] Se buscará la librería en el archivo \"%s\".\n", (char*) properties[PROP_LIBRARY_INDEX]);
        #endif
    }

    strcat(fileName, (char*) properties[PROP_LIBRARY_INDEX]);
    FILE *jsonFile = fopen(fileName, "r");
    if(!jsonFile) {
        #ifdef DEV_MODE 
            printf("[DEV MODE] La librería \"%s\" no existe.\n", (char*) properties[PROP_LIBRARY_INDEX]);
        #endif
        printf("La libreria que deseas emplear es inexistente.\n");
        return 300;
    }
    #ifdef DEV_MODE 
        printf("[DEV MODE] La librería \"%s\" ha sido resuelta con éxito. Puntero de archivo %p.\n",
            (char*) properties[PROP_LIBRARY_INDEX], jsonFile);
    #endif

    FILE *svgOutput = fopen("./icon.svg", "w+");
    if(!svgOutput) {
        #ifdef DEV_MODE
            printf("[DEV MODE] El ícono no se pudo generar.\n");
        #endif
        printf("No es posible generar el icono.\n");
        return 300;
    }
    #ifdef DEV_MODE
        printf("[DEV MODE] El ícono ha sido generado. Puntero a SVG: %p.\n", svgOutput);
    #endif

    fseek(jsonFile, 0, SEEK_END);
    #ifdef DEV_MODE
        printf("[DEV MODE] El puntero del archivo ha sido desplazado al final.\n");
    #endif

    fpos_t jsonFileLenght;
    fgetpos(jsonFile, &jsonFileLenght);
    #ifdef DEV_MODE
        printf("[DEV MODE] El tamaño del archivo es de %d carácteres.\n");
    #endif

    char *JSON_STRING = (char*) malloc(jsonFileLenght.__pos + STRING_OFFSET);
    rewind(jsonFile);
    fread(JSON_STRING, sizeof(char), jsonFileLenght.__pos, jsonFile);
    JSON_STRING[jsonFileLenght.__pos] = '\0';

    char *palette = strstr(JSON_STRING, KEY_PALETTE) + KEY_PALETTE_LEN;
    *(JSON_STRING = palette + FALSE_LEN) = '\0';
    JSON_STRING++;

    JSON_STRING = strstr(JSON_STRING, KEY_ICONS) + KEY_ICONS_LEN;
    int jsonLenght = strlen(JSON_STRING);

    fclose(jsonFile);

    int svgHeight, svgWidth;
    getDimensions(JSON_STRING, &svgWidth, &svgHeight);

    jsmn_parser p;
    jsmntok_t t[MAX_TOKENS];

    jsmn_init(&p);
    r = jsmn_parse(&p, JSON_STRING, strlen(JSON_STRING), t,
                    sizeof(t) / sizeof(t[0]));
    if (r < 0) {
        printf("Failed to parse JSON: %d\n", r);
        return 1;
    }

    if (r < 1 || t[0].type != JSMN_OBJECT) {
        printf("Object expected\n");
        return 1;
    }

    char *iconString = NULL;
    for (i = 1; i < r; i++) {
        if(jsoneq(JSON_STRING, &t[i], ((char*)properties[PROP_ICON_INDEX])) == JSON_EQUAL) {
            iconString = JSON_STRING + t[i].end;
            strchr(iconString, '}')[0] = '\0';
            break;
        }
    }

    if(!iconString) {
        printf("ICON NOT EXISTS.\n");
        return 300;
    }

    char *svgPath = strstr(iconString, KEY_BODY) + KEY_BODY_LEN - STRING_OFFSET;
    if((substr = strchr(svgPath, ','))) {
        (substr++)[0] = '\0';
        svgWidth = atoi(strstr(substr, KEY_WIDTH)+KEY_WIDTH_LEN);
    }

    fprintf(svgOutput, "<svg width=\"%s\" height=\"%s\" viewBox=\"0 0 %d %d\"", 
        (char*)properties[PROP_WIDTH_INDEX], (char*)properties[PROP_HEIGHT_INDEX], 
        svgWidth, svgHeight);

    int svgPathLen = strchr(svgPath, '>') - svgPath + 2;
    i = 1;
    r = 1;
    
    if(!strcmp(palette, FALSE_STR)){
        if(!strcmp((char*)properties[PROP_FILL_MODE_INDEX], FILL_MODE_SOLID))
            fprintf(svgOutput, " color=\"%s\">", (char*)properties[PROP_COLORS_INDEX]);
        else {
            int colorsNumber = 0;
            char **colors = strsplit((char*)properties[PROP_COLORS_INDEX], ',', &colorsNumber);
            int percentage = 100/colorsNumber;
            if(!strcmp((char*)properties[PROP_FILL_MODE_INDEX], FILL_MODE_LINEAR)){
                fprintf(svgOutput, "><defs><linearGradient id=\"linear\">");
                for(i = 1; i <= colorsNumber; i++)
                    fprintf(svgOutput, "<stop offset=\"%d\%\" style=\"stop-color:%s\"/>", 
                        i*percentage, colors[i - 1]);
                fprintf(svgOutput, "</linearGradient></defs>");
            } else if(!strcmp((char*)properties[PROP_FILL_MODE_INDEX], FILL_MODE_RADIAL)){
                fprintf(svgOutput, "><defs><radialGradient id=\"radial\">");
                for(i = 1; i <= colorsNumber; i++)
                    fprintf(svgOutput, "<stop offset=\"%d\%\" style=\"stop-color:%s\"/>", 
                        i*percentage, colors[i - 1]);
                fprintf(svgOutput, "</radialGradient></defs>");
            }
        }
        if(!strcmp((char*)properties[PROP_FILL_MODE_INDEX], FILL_MODE_LINEAR)) {
            i += 27;
            r = i;
            fprintf(svgOutput, "<path fill=\"url(#linear)\"");
        } else if(!strcmp((char*)properties[PROP_FILL_MODE_INDEX], FILL_MODE_RADIAL)) {
            i += 27;
            r = i;
            fprintf(svgOutput, "<path fill=\"url(#radial)\"");
        }
    } else {
        fprintf(svgOutput, ">");
    }

    for(; i < svgPathLen - 1; i++) {
        if(svgPath[i]=='\\'){
            r++;
            continue;
        }
        fprintf(svgOutput, "%c", svgPath[r++]);
    }

    fprintf(svgOutput, "</svg>");

    fclose(svgOutput);
    return EXIT_SUCCESS;
}