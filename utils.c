#include <string.h>
#include <stdlib.h>
#include "./jsmn.h"

const char* KEY_WIDTH = "width";
const int KEY_WIDTH_LEN = 5;
const char* KEY_HEIGHT = "height";
const int KEY_HEIGHT_LEN = 6;

#ifndef STRING_OFFSET
#define STRING_OFFSET 1
#endif // STRING_OFFSET

/**
 * Comprueba si el json recibido contiene la clave especificada.
 * @param json La localización en memoria del texto en formato JSON.
 * @param tok La información relativa al token que será comparado @see .
 * @param key La clave que está siendo buscada dentro del JSON.
 * @return Devuelve 0 sí las cadenas son iguales y -1 si no lo son.
*/
int jsoneq(const char *json, jsmntok_t *tok, const char *key) {
  if (
    /** 
     * Verificamos que el token evaluado sea una cadena de texto. 
    */
        tok->type == JSMN_STRING && 
    /**
     * Comprobamos que los tamaños de las cadenas correspondan.
    */
        (int)strlen(key) == tok->end - tok->start &&
    /**
     * Comparamos caracter a caracter ambas cadenas.
    */
        strncmp(json + tok->start, key, tok->end - tok->start) == 0) {
    return 0;
  }
  return -1;
}

/**
 * Remueve una parte del archivo JSON
 * @param string La cadena a la cual se le va a remover el contenido.
 * @param end El final de la cadena que será evaluada.
 * @param substring La subcadena que será substraida.
 * @return Devuelve la subcadena removiendo la subcadena indicada.
*/
char* removeJSONPart(char* string, int end, const char* substring) {
    /**
     * Se obtiene el tamaño de la cadena menos el desplazamiento de la misma.
    */
    int sublen = strlen(substring) - STRING_OFFSET;
    /**
     * Se guarda la referencia a la subcadena y se crea un puntero auxiliar para trabajar
     * a partir de este.
    */
    char *subidx = (char*) substring + sublen, *save = subidx;
    /**
     * Se trunca la cadena a la posición indicada como final.
    */
    string[--end] = '\0';
    /**
     * Se itera la cadena en reversa hasta que se encuentre la subcadena que se va a 
     * substraer. En caso de no corresponder vuelve a iniciar la iteración a partir de 
     * la nueva posición.
    */
    for(; end > 0 && subidx >= substring; ) {
        /**
         * Se evalúa que los caracteres correspondan entre sí.
        */
        if(*subidx == string[--end])
            subidx--;
        else
            subidx = save;
    }
    /**
     * Se generá el puntero de referencia a la clave que corresponde a la cadena sin el 
     * contenido posterior a la subcadena.
    */
    char *rest = ((char*)string) + end;
    /**
     * Se iteran los espacios en blanco de la cadena hasta encontrar el caracter "," 
     * (coma), una vez encontrado se detiene el ciclo.
    */
    for(;string[end]!=',';end--);
    /**
     * Se trunca la cadena para que el puntero resultante solo contenga la información que
     * debe contener.
    */
    string[end] = '\0';
    
    return rest;
}

/**
 * Corta una cadena a partir de un token especificado tantas veces como se repita el token.
 * @param string La cadena a ser partida en segmentos.
 * @param token El token que va a ser usado para partir la cadena en segmentos.
 * @param number un puntero donde se guardará el número de segmentos en que fue partida la cadena.
 * @returns Devuelve los segmentos de la cadena en formato de un arreglo de cadenas.
*/
char** strsplit(char* string, const char token, int* number) {
    /**
     * Se crean las variables que serán usadas para controlar la partición de la cadena.
     * n - número de segmentos
     * i - las iteraciones
    */
    int n = 1, i = 0;
    /**
     * Se itera sobre la cadena y se incrementa el contador de segmentos siempre que se encuentre el token.
    */
    while(string[i++])
        if(string[i]==token)
            n++;

    /**
     * Se crea el arreglo de subcadenas a partir del número de segmentos previamente obtenido.
    */
    char** colors = (char**) malloc(n*sizeof(char*));
    /**
     * Se inicializa la posición 0 del arreglo con el puntero original de la cadena.
    */
    colors[0] = string;
    /**
     * Se reinician los contadores.
    */
    i = 0;
    n = 1;
    /**
     * Se itera sobre los caracteres de la cadena de texto original. Y se buscan los tokens.
    */
    while(string[i]) {
        if(string[i] == token) {
            /**
             * En caso de que el caracter actual de la cadena sea el token, se trunca la cadena en la posición del token.
            */
            string[i] = '\0';
            /**
             * Se asigna a la siguiente posición libre del arreglo de segmentos de cadena el valor del siguiente segmento.
            */
            colors[n++] = string + i + STRING_OFFSET;
        }
        i++;
    }
    if(number != NULL)
        *number = n;
    return colors;
}

/**
 * Obtiene las dimensiones del svg original que ha solicitado el usuario.
 * @param substr Indica cual el svg que va a ser evaluado a continuación.
 * @param svgWidth Referencia a donde se almacenará el tamaño del actual svg en píxeles.
 * @param svgHeight Referencia a donde se almacenará el tamaño del actual svg en píxeles.
*/
void getDimensions(char *substr, int* svgWidth, int *svgHeight) {
    /**
     * Se obtiene el tamaño de la cadena que será evaluada y se inicia el contador de las dimensiones reconocidas.
    */
    int len = strlen(substr) - STRING_OFFSET, dimensions = 0;
    /**
     * Se crean punteros en formato de cadena de texto para las dimensiones del svg.
    */
    char *height, *width;
    /**
     * Se itera sobre la cadena siempre y cuando no hayan sido conocidas las dos dimensiones y no se sobrepase la cadena.
    */
    while(len >= 0 && dimensions < 2) {
        if(
            /**
             * Se verifica que la primera letra de la cadena 
            */
            substr[len] == KEY_HEIGHT[KEY_HEIGHT_LEN - STRING_OFFSET]
        )
            height = substr + len + 3*STRING_OFFSET;
    }
            
    if(height > width)
        *(height-KEY_HEIGHT_LEN - 3*STRING_OFFSET) = '\0';
    else
        *(width-KEY_WIDTH_LEN - 3*STRING_OFFSET) = '\0';
    *svgWidth = atoi(width);
    *svgHeight = atoi(height);
}