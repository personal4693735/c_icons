flags=-DDEV_MODE

library=noto
icon=prohibited
width=100
height=100
fill-mode=solid
colors=\#fff

run: output
	./output.out library=${library} icon=${icon} width=${width} height=${height} fill-mode=${fill-mode} colors=${colors}

output:
	gcc main.c -o output.out ${flags}