#ifndef JSMN_JSON
#define JSMN_JSON
#include "./libs/jsmn.h"
#endif

#ifndef _UTILS_JSON
#define _UTILS_JSON

const char* KEY_WIDTH = "width";
const int KEY_WIDTH_LEN = 5;
const char* KEY_HEIGHT = "height";
const int KEY_HEIGHT_LEN = 6;

extern int jsoneq(const char *json, jsmntok_t *tok, const char *key);
extern char* removeJSONPart(char* string, int end, const char* substring);
extern char** strsplit(char* string, const char token, int* number);
extern void getDimensions(char *substr, int* svgWidth, int *svgHeight);

#endif